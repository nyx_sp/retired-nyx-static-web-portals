"use strict";
var utteranceInputText;
var intentInputText;
var utteranceInputId = 0;
var entities = [];
var lastTrackedEntityId;
var intents = [];
var lastTrackedIntentId;
var utterances = [];
var isDragging = false;
var isMouseDown = false;
var $addedEntities;
var dragNextEmpty = true;
var scanNextEmpty = true;
var textPos;

var curUtterance; //Contains an object of currently selected utterance
var $curSelectedWords; //Contains a jquery object of currently selected utterance
var $curUtteranceDisplay; //Contains a jquery object of the current utterance row
var $curUtteranceRows = []; //Contains an of aray jquery objects of all the rows

var hasBingEntity = false; //Global object to keep track of whether bing entity
                            //is being used
var bingEntityErrorMsg = "";
var bingEntityCount = 0;

// Dashboard Extension start
var entityResponse = [];
var curSelectedPlainText = "";

function checkIfEntityResponseExists(obj) {
  var pos = -1;
  for (var i = 0; i < entityResponse.length; i++) {
    if ((entityResponse[i].text == obj.text)
        && (entityResponse[i].entity.getEntityName() == obj.entity.getEntityName())) {
      console.log("found!");
      pos = i;
      break;
    }
  }
  return pos;
}

function collectEntityResponse(entityPassed) {
  var obj = { text: curSelectedPlainText, message: $("#entityMessageInput").val(), entity: entityPassed };
  var pos = checkIfEntityResponseExists(obj);
  if (pos != -1) {
    entityResponse[pos] = obj;
  } else {
    entityResponse.push(obj);
  }
}
// Dashboard Extension end

function collectUtteranceIntentValues(intentInputText){
  utteranceInputText = $("#utteranceInput").val();
  // Intent values overidden by jquery autosuggest plugin
  // check plugin at the bottom
  if (utteranceInputText == "")
    return;
  var utteranceAccepted = new Utterances();
  utteranceAccepted.setUtteranceText(utteranceInputText);
  utteranceAccepted.explodeText();
  utteranceAccepted.setIntent(intentInputText);
  curUtterance = utteranceAccepted;
  populatePreviewAreaContent(utteranceAccepted);
  emptyTextBoxes();
}

function emptyTextBoxes() {
  $("#utteranceInput").val("");
  $("#intentInput").val("");
  $("#entitiesInput").val("");
}

function displayEnteredIntents() {
  $("#intentGroup").empty();
  for (var i = 0; i < intents.length; i++){
    $("#intentGroup").append('<p class="entityBlock">' + intents[i].getIntentName() + '</p>');
  }
}

function displayEnteredEntities() {
  $("#entityGroup").empty();
  if (entities.length != 0) {
    $("#attrDdl").empty();
  }
  for (var i = 0; i < entities.length; i++){
    $("#entityGroup").append('<p class="entityBlock">' + entities[i].getEntityName() + '</p>');
    $("#attrDdl").append('<option value="' + entities[i].getEntityId() + '">' +
      entities[i].getEntityName() + '</option>');
  }
}

function populatePreviewAreaContent(utteranceAccepted) {
  var $utteranceBlock = $('<div class="utteranceBlock"></div>');
  var $utteranceIntent = $('<p class="utteranceIntent">' + utteranceAccepted.getIntent() + '</p><hr/>');
  var $utteranceExp = $('<p class="utteranceExploded"></p>');
  var et = utteranceAccepted.explodeText();
  for (var i = 0; i < et.length; i++){
    $utteranceExp.append(et[i]);
  }
  $utteranceBlock.append($utteranceIntent).append($utteranceExp);
  $("#previewAreaContent").empty().append($utteranceBlock);
}

function populateEnteredContent($displayBlock){
  $("#trainingCount").html($curUtteranceRows.length);
  for (var i = 0; i < $curUtteranceRows.length; i++){
    $("#displayUtterances").prepend($curUtteranceRows[i]);
  }
}

function enteredValuesClickEventHandler($instance){
  console.log($instance.text());
}

function enteredValuesHoverEventHandler($instance){
  $instance.css("background-color","white");
  $instance.css("color","#2298fc");
}

function enteredValuesHoverEventHandlerLeave($instance){
  // $instance.css("background-color", "transparent");
  $instance.css("background-color","");
  $instance.css("color","");
}

(function ($, undefined) {
    $.fn.getCursorPosition = function() {
        var el = $(this).get(0);
        var pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})(jQuery);

function Entity() {
  this.data = lastTrackedEntityId;
  this.value = "";
  this.isOptional = false;
  this.errMsg = "";
  lastTrackedEntityId++;
}

Entity.prototype = {
  getEntityId: function() {
    return this.data;
  },
  getEntityName: function() {
    return this.value;
  },
  setEntityName: function(name) {
    this.value = name;
  },
  getIsOptional: function() {
    return this.isOptional;
  },
  setIsOptional: function(bool) {
    this.isOptional = bool;
  },
  getErrMsg: function() {
    return this.errMsg;
  },
  setErrMsg: function(text) {
    this.errMsg = text;
  }
}

function EntityUtterance() {
  this.entity = "";
  this.startPos = -1;
  this.endPos = -1;
}

EntityUtterance.prototype = {
  getEntityName: function() {
    return this.entity;
  },
  setEntityName: function(entity) {
    this.entity = entity;
  },
  getStartPos: function() {
    return this.startPos;
  },
  setStartPos: function(pos) {
    this.startPos = pos;
  },
  getEndPos: function() {
    return this.endPos;
  },
  setEndPos: function(pos) {
    this.endPos = pos;
  }
}

function Intent() {
  this.data = lastTrackedIntentId;
  this.value = "";
  this.bingEntity = [];
  this.entity = [];
  lastTrackedIntentId++;
}

Intent.prototype = {
  getIntentId: function() {
    return this.data;
  },
  getIntentName: function() {
    return this.value;
  },
  setIntentName: function(name) {
    this.value = name;
  },
  getBingEntity: function() {
    return this.bingEntity;
  },
  addBingEntity: function(text) {
    this.bingEntity.push(text);
  },
  setBingEntity: function(text) {
    this.bingEntity = text;
  },
  addEntity: function(entity) {
    this.entity.push(entity);
  },
  setEntity: function(entitySet) {
    this.entity = entitySet;
  },
  getEntities: function() {
    return this.entity;
  }
}

function Utterances() {
  this.Id = utteranceInputId;
  this.UtteranceText = "";
  this.Intent = "";
  this.Entities = [];
  this.NyxEntities = [];
  this.ExplodedText = [];
  this.DisplayString;
  utteranceInputId++;
};

Utterances.prototype = {
  getNyxEntities: function() {
    return this.NyxEntities;
  },
  addNyxEntity: function(nyxentity) {
    this.NyxEntities.push(nyxentity);
  },
  getUtteranceText: function() {
    return this.UtteranceText;
  },
  setUtteranceText: function(text) {
    this.UtteranceText = text;
  },
  getEntities: function() {
    return this.Entities;
  },
  addEntity: function(text) {
    this.Entities.push(text);
  },
  clearEntities: function() {
    this.Entities = [];
  },
  setIntent: function(text) {
    this.Intent = text;
  },
  getIntent: function() {
    return this.Intent;
  },
  explodeText: function(){
    this.ExplodedText = this.UtteranceText.split(" ");
    var $displayText = $('');
    var $spanText = $('');
    textPos = 0;
    this.ExplodedText.forEach(function(entry){
      $spanText = $('<span class="content" value="' + textPos + '">' + entry + '</span><span class="space"> </span>');
      $spanText.bind({
        mouseenter: function(){
          if(!($(this).parent().hasClass("groupSelected"))){
            $(this).toggleClass("hoverEntity");
          }
        },
        mouseleave: function(){
          if ($(this).hasClass("hoverEntity")){
            $(this).toggleClass("hoverEntity");
          }
        },
        mousedown: function() {
          if(!($(this).parent().hasClass("groupSelected"))){
            isMouseDown = true;
            $(this).toggleClass("selectedEntity");
          }
        },
        mouseover: function() {
          if (isMouseDown) {
            $(this).toggleClass("selectedEntity");
          }
        },
        mousemove: function() {
          isDragging = true;
        },
        mouseup: function() {
          isDragging = false;
          isMouseDown = false;
          if ($(this).parent().hasClass("groupSelected")){
            $(this).parent().children("span").toggleClass("selectedEntity");
            $(this).parent().find(".entityLabel").remove();
            $(this).unwrap();
          } else {
            var $groupSelected = $('<span class="groupSelected"></span>');
            $groupSelected.bind({
              mouseenter: function(){
                $(this).toggleClass("hoverSelectedEntity");
                if($(this).hasClass("entity")){
                  $(this).find(".entityLabel").removeClass("invisible");
                }
              },
              mouseleave: function(){
                if ($(this).hasClass("hoverSelectedEntity")){
                  $(this).toggleClass("hoverSelectedEntity");
                }
                if($(this).hasClass("entity")){
                  $(this).find(".entityLabel").addClass("invisible");
                }
              }
            });

            if ($(this).prev().hasClass("groupSelected") || $(this).next().hasClass("groupSelected")){
              //// this function connects two adjacent selected divs together
              //// it was not implemented as there is a bug that fails to remove entityLabel div
              // console.log("A");
              // $(this).nextUntil(":not(.groupSelected)").each(function(){
              //   $(this).children().unwrap();
              //   scanNextEmpty = false;
              // });
              // if (!scanNextEmpty) {
              //   console.log("A1");
              //   $(this).prevUntil(":not(.groupSelected)").each(function() {
              //     console.log("hi");
              //     console.log($(this));
              //     $(this).parent().find(".entityLabel").remove();
              //     $(this).children().unwrap();
              //   });
              //   scanNextEmpty = true;
              // } else {
              //   console.log("A2");
              //   console.log($(this));
              //   $(this).parent().find(".entityLabel").remove();
              //   $(this).prevUntil(":not(.groupSelected)").each(function() {
              //     $(this).children().unwrap();
              //   });
              // }
              // // $(this).parent().prevUntil(":not(.groupSelected)").each(function() {
              // //   $(this).children().unwrap();
              // // });
              // $(this).nextUntil(":not(.selectedEntity)").each(function(){
              //   scanNextEmpty = false;
              // }).addBack().wrapAll($groupSelected);
              // if (!scanNextEmpty) {
              //   console.log("A3");
              //   $(this).unwrap();
              //   $(this).nextUntil(":not(.selectedEntity)").addBack().wrapAll($groupSelected);
              //   $(this).parent().prevUntil(":not(.selectedEntity)").addBack().wrapAll($groupSelected);
              //   $(this).unwrap();
              //   $(this).append('<div class="entityLabel"></div>');
              //   scanNextEmpty = true;
              // } else {
              //   console.log("A4");
              //   $(this).parent().prevUntil(":not(.selectedEntity)").addBack().wrapAll($groupSelected);
              //   $(this).unwrap();
              //   $(this).parent().append('<div class="entityLabel"></div>');
              // }
              ////$(this).parent().prevUntil(":not(.groupSelected)").wrapAll($groupSelected);
            } else {
              console.log("B");
              $(this).nextUntil(":not(.selectedEntity)").each(function(){
                dragNextEmpty = false;
              }).addBack().wrapAll($groupSelected);
              if (!dragNextEmpty) {
                console.log("B1");
                $(this).parent().prevUntil(":not(.selectedEntity)").wrapAll($groupSelected);
                dragNextEmpty = true;
                $(this).parent().append('<div class="entityLabel"></div>');
              } else {
                console.log("B2");
                $(this).parent().prevUntil(":not(.selectedEntity)").addBack().wrapAll($groupSelected);
                $(this).unwrap();
                $(this).parent().append('<div class="entityLabel invisible"></div>');
              }
              // $(this).nextUntil(":not(.selectedEntity)").prevUntil(":not(.selectedEntity)").andSelf().wrapAll($groupSelected);
            }

            // $(this).parent().find(".selectedEntity+.selectedEntity").each(function(){
            //   // if ($(this).parent().hasClass(".groupSelected")){
            //   //   $(this).unwrap();
            //   // }
            //   //$addedEntities = $($(this).prev().addBack());
            // })

            //$addedEntities.wrapAll($groupSelected);
            //$('.selectedEntity').wrapAll($groupSelected);
            var selectedPlainText = $(this).parent().find("span").text();
            populateAttrOverlayContent(selectedPlainText);
            toggleAttrOverlay();
          } //end of if parent hasClass groupSelected
          $curSelectedWords = $(this).parent();
          curSelectedPlainText = selectedPlainText;
        } //end of mouseup
      });
      $spanText.addClass("noSelect");
      $displayText = $displayText.add($spanText);
      textPos++;
    });//End of forEach
    console.log($displayText);
    this.DisplayString = $displayText;
    return $displayText;
  },
  getDisplayString: function() {
    return this.DisplayString;
  },
  getId: function() {
    return this.Id;
  }
};

function toggleAttrOverlay() {
  $("#attrOverlay, .bgOverlay").toggleClass("invisible");
  $("#mainContent").toggleClass("blurOverlay");
}

function toggleIntentOverlay() {
  $("#intentOverlay, .bgOverlay").toggleClass("invisible");
  $("#mainContent").toggleClass("blurOverlay");
}

function toggleEntityPropertiesOverlay() {
  $("#attrOverlay").toggleClass("invisible");
  $("#entityPropertiesOverlay").toggleClass("invisible");
  // $("#mainContent").toggleClass("blurOverlay");
}

function toggleExportOverlay() {
  $("#exportOverlay, .bgOverlay").toggleClass("invisible");
  $("#mainContent").toggleClass("blurOverlay");
}

function populateAttrOverlayContent(selectedContent) {
  $("#attrOverlaySelectedText").html('<span class="hoverSelectedEntity selectedEntityOverlay">'
    + selectedContent + '</span>');
  checkAndDisplayEntityResponse(selectedContent);
}

function checkAndDisplayEntityResponse(selectedContent) {
  $("#changesWarningMsg").addClass("invisible");
  console.log('selected val:');
  console.log($("#attrDdl option:selected").val());
  var pos = -1;
  if ($("#attrDdl option:selected").val() != -1) {
    for (var i = 0; i < entities.length; i++) {
      if ($("#attrDdl option:selected").text() == entities[i].getEntityName()) {
        pos = i;
        break;
      }
    }
  }
  var obj = { text: selectedContent, message: "", entity: entities[pos] };
  var response = checkIfEntityResponseExists(obj);
  console.log("response:");
  console.log(response);
  if (response != -1) {
    console.log(entityResponse[response].message);
    $("#entityMessageInput").val(entityResponse[response].message);
    $("#changesWarningMsg").removeClass("invisible");
  } else {
    $("#entityMessageInput").val("");
  }
}

function populateIntentOverlayContent(selectedContent) {
  $("#intentOverlaySelectedText").html('<span class="hoverSelectedEntity selectedEntityOverlay">'
    + selectedContent + '</span>');
}

function populateEntityPropertiesOverlayContent(selectedContent) {
  $("#entityPropertiesOverlaySelectedText").html('<span class="hoverSelectedEntity selectedEntityOverlay">'
    + selectedContent + '</span>');
}

// function formatDataToLuis() {
//   var obj = {
//     "luis_schema_version": ,
//     "name": "",
//     "desc": "",
//     "culture": "en-us",
//     "intents":
//   }
// }

function LuisDataSet() {
  this.luis_schema_version = "1.0.0";
  this.name = "";
  this.desc = "";
  this.culture = "en-us";
  this.intents = [];
  this.entities = [];
  this.bing_entities = [];
  this.model_features = [];
  this.regex_features = [];
  this.utterances = [];
};

LuisDataSet.prototype = {
  getUtteranceText: function() {
    return this.UtteranceText;
  },
  addBingEntity: function(entity) {
    this.bing_entities.push(entity);
  },
  addEntity: function(entity) {
    this.entities.push(entity);
  },
  addIntent: function(intent) {
    this.intents.push(intent);
  },
  addUtterance: function(utterances) {
    this.utterances.push(utterances);
  },
  setProjectName: function(text) {
    this.name = text;
  },
  setProjectDesc: function(text) {
    this.desc = text;
  }
}

function NyxDataRow() {
  this.intent = "";
  this.entities = [];
  this.entityResponse = [];
}

NyxDataRow.prototype = {
  getIntentName: function() {
    return this.intent;
  },
  setIntent: function(text) {
    this.intent = text;
  },
  addEntity: function(entity) {
    this.entities.push(entity);
  },
  setEntityResponse: function(arr) {
    this.entityResponse = arr;
  }
}

function NyxEntity() {
  this.entity = "";
  this.errorMessage = "";
  this.isCompulsory = false;
}

NyxEntity.prototype = {
  getEntityName: function() {
    return this.entity;
  },
  setEntityName: function(text) {
    this.entity = text;
  },
  setErrorMessage: function(text) {
    this.errorMessage = text;
  },
  setIsCompulsory: function(bool) {
    this.isCompulsory = bool;
  }
}

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

$(document).ready(function(){
  lastTrackedEntityId = 0;
  lastTrackedIntentId = 0;

  $("#addUtteranceBtn").click(function() {
    console.log("INTENT BEFORE");
    console.log(intents);
    $curUtteranceDisplay = $("#previewAreaContent").children();
    $curUtteranceRows.push($curUtteranceDisplay);

    populateEnteredContent();
    // // for (var i = 0; i < curUtterance.getNyxEntities().length; i++) {
    // //   for (var j = 0; j < intents.length; j++) {
    // //     if (curUtterance.getIntent() == intents[i].getIntentName()){
    // //       intents[i].setEntity(curUtterance.getNyxEntities());
    // //       break;
    // //     }
    // //   }
    // // }
    //
    utterances.push(curUtterance);
    var intentPos = 0;
    for (var i = 0; i < intents.length; i++) {
      console.log("A");
      // Loop through all intents
      if (intents[i].getIntentName() == curUtterance.getIntent()) {
        intentPos = i;
        console.log("B");
        for (var k = 0; k < curUtterance.getNyxEntities().length; k++) {
          console.log('k:' + k, curUtterance.getNyxEntities());
          console.log(curUtterance);
          for (var j = 0; j < intents[i].getEntities().length; j++) {
            console.log('j:' + j, intents[i].getEntities());
            console.log(curUtterance);
            if(intents[i].getEntities()[j].getEntityName() == curUtterance.getNyxEntities()[k].getEntityName()) {
              intents[i].getEntities().splice(j, 1);
            }
          }
        }
      }
    }
    var curUtteranceEntities = curUtterance.getNyxEntities();
    var curUtteranceEntitiesToCheck = curUtteranceEntities;
    var matchCount;
    for (var i = 0; i < curUtteranceEntitiesToCheck.length; i++) {
      matchCount = 0;
      for (var j = 0; j < curUtteranceEntities.length; j++) {
        if (curUtteranceEntitiesToCheck[i].getEntityName() == curUtteranceEntities[j].getEntityName()){
          matchCount++;
          if (matchCount == 2) {
            curUtteranceEntities.splice(j , 1);
            matchCount--;
          }
        }
      }
    }

    console.log("line 578");
    console.log(curUtteranceEntities);
    intents[intentPos].setEntity(curUtteranceEntities);
    console.log("INTENT AFTER");
    console.log(intents);
  });

  $("#dismissBtn").click(function() {
    toggleAttrOverlay();
    var entA = new EntityUtterance();
    var ddlText = $("#attrDdl option:selected").text();
    console.log(ddlText);
    var ddlVal = $("#attrDdl option:selected").val();
    if (ddlVal != -1){
      entA.setEntityName(ddlText);
      $curSelectedWords.addClass("entity");
      $curSelectedWords.attr("value", ddlText)
      console.log(curUtterance);
      $curSelectedWords.find(".entityLabel").html(ddlText);
      var firstTime;
      firstTime = true;
      var firstVal;
      var lastVal;
      $curSelectedWords.find(".content.selectedEntity").each(function() {
        if (firstTime) {
          firstVal = $(this).attr("value");
          firstTime = false;
        } else {
          lastVal = $(this).attr("value");
        }
        console.log(firstVal + " + " + lastVal);
      });
      if (lastVal == null) {
        lastVal = firstVal;
      }
      entA.setStartPos(firstVal);
      entA.setEndPos(lastVal);
      curUtterance.addEntity(entA);
      console.log("ENT A IS:");
      console.log(entA);
      console.log("ALL ENTITIES:");
      console.log(entities);
      for (var i = 0; i < entities.length; i++) {
        console.log(entities[i].getEntityName());
        console.log(entA.getEntityName());
        if (entities[i].getEntityName() == entA.getEntityName()) {
          console.log("hit2");
          curUtterance.addNyxEntity(entities[i]);
          collectEntityResponse(entities[i]);
          break;
        }
      }
      console.log("CURUTTERANCE AFTER ENTITY");
      console.log(curUtterance);
    }
  });

  $("#dismissIntentPropertiesBtn").click(function() {
    var int = new Intent();
    console.log($("#intentInput").val());
    int.setIntentName($("#intentInput").val());
    if ($("#usesBingDateTimeCb").is(":checked")){
      bingEntityCount++;
      hasBingEntity = true;
      bingEntityErrorMsg = $("#errorMsgBingInput").val();
      int.addBingEntity("dateTime");
    } else {
      bingEntityCount--;
      if (bingEntityCount == 0){
        hasBingEntity = false;
      }
      int.setBingEntity("");
    }
    intents.push(int);
    displayEnteredIntents();
    collectUtteranceIntentValues($("#intentInput").val());
    toggleIntentOverlay();
  });

  $("#dismissEntityPropertiesBtn").click(function() {
    var ent = new Entity();
    ent.setEntityName($("#entitiesInput").val());
    if ($("#isOptionalCb").is(":checked")){
      ent.setIsOptional(true);
    } else {
      ent.setIsOptional(false);
    }
    if ($("#errorMsgInput").val() != ""){
      ent.setErrMsg($("#errorMsgInput").val());
    }
    entities.push(ent);
    console.log(entities);
    displayEnteredEntities();
    toggleEntityPropertiesOverlay();
    $("#attrDdl option:last").attr("selected","selected");
    $("#entitiesInput").val("");
    checkAndDisplayEntityResponse(curSelectedPlainText);
  });

  $("#utteranceInput").keypress(function(e) {
    if(e.which == 13 && $(this).val() != "") {
        $("#intentInput").focus().keypress(function(e) {
          if(e.which == 13 && $(this).val() != "") {
            collectUtteranceIntentValues();
            $("#entitiesInput").focus().keypress(function(e) {
              if(e.which == 13 && $(this).val() != "") {
                $("#utteranceInput").focus();
              }
            })
          }
        });
    }
    //console.log($("#utteranceInput").getCursorPosition());
  });


  $("#intentInput").devbridgeAutocomplete({
      lookup: intents,
      autoSelectFirst: true,
      preserveInput: true,
      triggerSelectOnValidInput: false,
      onSelect: function(suggestion) {
        if (suggestion.data == -2){
          populateIntentOverlayContent($(this).val());
          toggleIntentOverlay();
        } else {
          collectUtteranceIntentValues(suggestion.value);
        }
      }
  });

  $("#entitiesInput").devbridgeAutocomplete({
      lookup: entities,
      autoSelectFirst: true,
      preserveInput: true,
      triggerSelectOnValidInput: false,
      onSelect: function(suggestion) {
        if (suggestion.data == -2){
          populateEntityPropertiesOverlayContent($(this).val());
          toggleEntityPropertiesOverlay();
        } else {
          $(this).val("");
        }
      }
  });

  $("#exportBtn").click(function() {
    toggleExportOverlay();
    console.log("entities:");
    console.log(entities);
    console.log("intents:");
    console.log(intents);
    console.log("utterances:");
    console.log(utterances);

    // var intentMatch;
    // var entityMatch;
    // var entityNameToCompare;
    // for (var i = 0; i < utterances.length; i++) {
    //   for (var j = 0; j < utterances[i].getEntities().length; j++) {
    //     for (var k = 0; k < nyxDataSet.length; k++) {
    //       for (var l = 0; l < nyxDataSet[k].getEntities().length; l++) {
    //         if (utterances[i].getEntities()[j].getEntityName() ==
    //               nyxDataSet[k].getEntities()[l].getEntityName()){
    //           entityMatch = true;
    //           break;
    //         }
    //       }
    //     }
    //     if (!entityMatch) {
    //       var ndr = new NyxDataRow();
    //       var ne = new NyxEntity();
    //       ne.setEntityName(utterances[i].getEntities()[j].getEntityName());
    //       ne.setErrorMessage(utterances[i].getEntities()[j].getEntityName())
    //     }
    //   }
    // }
  });

  $("#dismissExportOverlayBtn").click(function() {
    toggleExportOverlay();
  });

  $("#downloadLuisBtn").click(function() {
    // populate luis dataset
    var l = new LuisDataSet();
    if (hasBingEntity) {
      l.addBingEntity("dateTime");
    }
    for (var i = 0; i < entities.length; i++){
      l.addEntity(entities[i].getEntityName());
    }
    l.setProjectName($("#luisNameInput").val());
    l.setProjectDesc($("#luisDescInput").val());
    for (var i = 0; i < intents.length; i++){
      l.addIntent(intents[i].getIntentName());
    }
    for (var i = 0; i < utterances.length; i++){
      var tempObject = {
        "text": utterances[i].getUtteranceText(),
        "intent": utterances[i].getIntent(),
        "entities": utterances[i].getEntities()
      };
      l.addUtterance(tempObject);
    }
    download('luis.txt', JSON.stringify(l));
  });

  $("#downloadNyxBtn").click(function() {
    // populate nyx dataset
    var nyxDataSet = [];
    var nye;
    var ndr;
    var ents = [];
    for (var i = 0; i < intents.length; i++) {
      ndr = new NyxDataRow();
      ndr.setIntent(intents[i].getIntentName());
      console.log(intents);
      ents = intents[i].getEntities();
      for (var j = 0; j < ents.length; j++){
        nye = new NyxEntity();
        console.log(ents);
        nye.setEntityName(ents[j].getEntityName());
        nye.setErrorMessage(ents[j].getErrMsg());
        if (ents[j].getIsOptional()) {
          nye.setIsCompulsory(false);
        } else {
          nye.setIsCompulsory(true);
        }
        ndr.addEntity(nye);
      }
      if (intents[i].getBingEntity().length != 0){
        nye = new NyxEntity();
        nye.setEntityName("builtin.datetime.date");
        nye.setErrorMessage(bingEntityErrorMsg);
        nye.setIsCompulsory(true);
        ndr.addEntity(nye);
      }
      nyxDataSet.push(ndr);
    } // end of for loop
    var tempEntityResponse = [];
    for (var i = 0; i < entityResponse.length; i++) {
      tempEntityResponse.push({"text" : entityResponse[i].text,
        "message" : entityResponse[i].message,
         "entity" : entityResponse[i].entity.getEntityName() });
    }
    ndr.setEntityResponse(tempEntityResponse);
    download('nyxbot.json', JSON.stringify(nyxDataSet));
  });

  $("#attrDdl").change(function(){checkAndDisplayEntityResponse(curSelectedPlainText);});
  //
  // curUtterance = new Utterances();
  // curUtterance.setUtteranceText("jj v v qwf qwf wqvr");
  // curUtterance.setIntent("uur");
  //
  // populatePreviewAreaContent(curUtterance);

  // Used javascript to set the css for autocomplete because
  // the jquery autocomplete plugin uses inline styling, which has higher priority
  // plugin source code can be found here: https://github.com/devbridge/jQuery-Autocomplete
  $(".autocomplete-suggestions").css("border", "1px solid black");
  $(".autocomplete-no-suggestion").text('Add New');
});
